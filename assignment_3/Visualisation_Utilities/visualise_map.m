% Set up figure window and plot obstacles and various useful map elements.
clf; hold on;
plot([0,5,5,6,6,7,7,11]', [0,0,1,1,2,2,3,3]');
plot(10,3.2, 'rx','LineWidth',2,'MarkerSize',10)
plot(4.1,0.3, 'g+','LineWidth',2,'MarkerSize',10)
axis([0,11,0,4]);

if useUNTUNED
    title(['Fuzzy Car Sim, Start Angle = ', num2str(angleStart),'^o, Untuned FLC']);
else
    title(['Fuzzy Car Sim, Start Angle = ', num2str(angleStart),'^o, Tuned FLC']);
end

