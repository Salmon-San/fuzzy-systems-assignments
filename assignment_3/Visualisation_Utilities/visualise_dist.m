function [ distVizHandle ] = visualise_dist( distVizHandle, carState )
%VISUALISE_DIST Plots the measured distance between the car and closest
%obstacle, on each cartesian axis.

if exist('distVizHandle'); 
    delete(distVizHandle); 
end

distVizHandle = plot( ...
    [carState.carX,carState.carX+carState.Dh], [carState.carY, carState.carY],'m--',...
    [carState.carX,carState.carX], [carState.carY, carState.carY-carState.Dv],'m--');
end

