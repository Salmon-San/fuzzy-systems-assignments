function [ carVizHandles ] = visualise_car( carVizHandles, carState )
%VIZUALISE_CAR plots the car position and direction on the map
%   This functions clears previous plot elements, by passing the
%   proper handles-struct.

% Clear Previous Car graphic; 
directionHandle = carVizHandles.directionHandle;
if exist('directionHandle'); 
    delete(directionHandle); 
end
posHandle = carVizHandles.posHandle;
if exist('posHandle'); 
    delete(posHandle); 
end

% %Plot New Car Graphic
carVizHandles.directionHandle = quiver(carState.carX,carState.carY, ...
    cos(carState.theta*pi/180)/2, sin(carState.theta*pi/180)/2, 'g', 'LineWidth',2, 'MarkerSize',10);
carVizHandles.posHandle = plot(carState.carX, carState.carY, 'go');

% Plot trajectory trace
plot(carState.carX,carState.carY,'r')

end

