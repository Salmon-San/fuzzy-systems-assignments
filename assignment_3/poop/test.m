clear;
theta = -360:360;
thetaA = theta;

for i=1:length(theta)
    if theta(i) > 180
        theta(i) = theta(i) - 360;
    elseif theta(i) < -180
        theta(i) = 360 + theta(i) ;
    end
end

plot([thetaA',theta'])
title('Angle Range Fix');
ylabel('Angle [deg]');
legend('Regular Angle', 'FixedAngle');