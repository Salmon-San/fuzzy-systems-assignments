function [ dTheta ] = car_controller( carState )
%CAR_CONTROLLER is in charge of the FIS evaluation and the FLC's input
%saturation.
%   Angle theta is periodically bounden in [-180,180], so that the
%   Dh,Dv is bounded in [0,1]. 
%   The function returns dTheta [deg/sec];

Dh = carState.Dh;
Dv = carState.Dv;

% Saturate Distance Measurements
if Dh > 1
    Dh = 1;
elseif Dh <= 0
    Dh = 0;
end
if Dv > 1
    Dv = 1;
elseif Dv <= 0
    Dv = 0;
end

% Normalise Angle. Otherwise the FLC will go bananas due to limited
% input range.
if carState.theta > 180
    carState.theta = carState.theta - 360;
elseif carState.theta < -180
    carState.theta = 360 + carState.theta ;
end

dTheta = evalfis([Dv,Dh,carState.theta], carState.carFLC); %[deg/sec]
end

