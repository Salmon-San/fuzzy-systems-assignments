function [ collided ] = check_collision( carState )
%CHECK_COLLISION checks whether the car has collided on any obstacle of the
%given map. Returns 0for no collision.

if (carState.carY<1) && (carState.carX>5)
    collided = 1;
elseif (carState.carY<2) && (carState.carX>6)
    collided = 1;
elseif (carState.carY<3) && (carState.carX>7)
    collided = 1;
else
    collided = 0;
end

end