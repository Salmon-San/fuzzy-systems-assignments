function [ carState ] = update_car_state( carState, dTheta , DT)
%UPDATE_CAR_STATE calculates the next position and orientation of the car.
%   The recursive solution of the car's state is done by integrating the
%   velocity on each axis with a simple euler integrator x(n+1)=x(n)+xd*DT. 

carVelocity = 0.05; % [m/s] Constant

carState.theta = carState.theta + dTheta*DT;
carState.carX = carState.carX + carVelocity*cos(pi/180*carState.theta)*DT;
carState.carY = carState.carY + carVelocity*sin(pi/180*carState.theta)*DT;

end

