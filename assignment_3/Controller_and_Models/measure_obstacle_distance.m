function [ carState ] = measure_obstacle_distance( carState )
%MEASURE_OBSTACLE_DISTANCE Simualtes the cartesian distance measurement of
% closest obstacles. 

if carState.carX < 5
    Dv = carState.carY - 0;
elseif carState.carX < 6
    Dv = carState.carY - 1;
elseif carState.carX < 7
    Dv = carState.carY - 2;
else
    Dv = carState.carY - 3;
end

if carState.carY < 1
    Dh =  5 - carState.carX;
    
elseif carState.carY < 2
    Dh =  6 - carState.carX;
    
elseif carState.carY < 3
    Dh =  7 - carState.carX;
else
    % We Assume there are no obstacles inthe X-direction, after the last step.
    Dh = 100; %+inf;
end

carState.Dh = Dh;
carState.Dv = Dv;

end


