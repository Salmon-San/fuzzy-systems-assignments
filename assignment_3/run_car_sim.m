function [CAR_X, CAR_Y] = run_car_sim(angleStart, animationON, useUNTUNED)
% RUN_SIM_CAR executes the simulation of the fuzzy car system. 
% 
% The Simulation Parameters are the following:
%   angleStart [deg] - The initial angle of the vehicle
% 
%   animationON - Set to FALSE for no online visualisation. The final 
%       results will appear at the end of the simulation. 
% 
%   useUNTUNED  - Set to TRUE to use the untuned version of the FLC. The
%       fuzzy rules will be the same, only the MF-parameters will be different.
% 
% Returns: The final [CAR_X, CAR_Y]

%% Sim init
if useUNTUNED
    disp('[WARNING]: Untuned FLC in use');
    carFLC = readfis('carFLC_untuned');
else
    disp('[MESSAGE]: Tuned FLC in use');
    carFLC = readfis('carFLC_tuned');
end

% Simulation Step Time
DT = 0.05; %[sec]

% This struct holds all the controllers input,output as well as the model's
% states. Angle theta is in degrees.
carState = struct('carX',4.1,'carY',0.3, 'theta', angleStart, 'Dh',1,'Dv',1, 'carFLC', carFLC);

% This array stores the coordinates of the car for later vizualisation
CAR_POS = [carState.carX,carState.carY,carState.theta];

% This script sets up the figure window and plots all map elements and
% obstacles.
if (animationON) visualise_map; end

% This structs stores some gui handles for the visualisation process and
% proper animation of the car's motion.
carVizHandles = struct('directionHandle', plot(1,1), 'posHandle', plot(1,1));

% This is the handle of the visualised measured distance between the car
% and the closest obstacle
distVizHandle = plot(1,1);

%% Sim run
tic;
i = 1;
while 1==1
    % Check Target Completion
    if carState.carX > 10
        disp('[SUCCESS]: Car has arrived to its Destination!');
        disp(['  ...You just need to walk ', num2str(carState.carY-3.2), ' meters']);
        break;
    end
    if carState.carX > 11 || carState.carX < 0 || ...
            carState.carY > 4 || carState.carY < 0
        disp('[ERROR]: Car out of map bounds');
        break;
    end
    
    % Controller: Sensor measurement phase: Estimate DH and DV
    carState = measure_obstacle_distance(carState);
    
    % Controller: Actuation phase: The FLC estimateds the steering of the vehicle.
    dTheta = car_controller(carState);
    
    % Simulation: Update car state and check for collision with obstacles
    carState = update_car_state(carState, dTheta, DT);
    CAR_POS = [CAR_POS; carState.carX,carState.carY, carState.theta];
    collided = check_collision(carState);
    if collided
        disp('[ERROR]: Car Collided on Wall. Terminating simulation.');
        break;
    end
    
    % Visualisation: Disable with animatioON variable set to false
    i = i +1;
    if animationON
        % For sped up animation, we plot every 10 cycles...
        if mod(i,10)==0
            carVizHandles = visualise_car(carVizHandles, carState);
            distVizHandle = visualise_dist(distVizHandle, carState);
            drawnow;
        end
    end
end
disp(['[SIM_END]: Simulated time: ', num2str((i-1)*DT), ' seconds']);
toc

CAR_X = CAR_POS(:,1);
CAR_Y = CAR_POS(:,2);

if ~animationON
    visualise_map
    carVizHandles = struct('directionHandle', plot(1,1), 'posHandle', plot(1,1));
    distVizHandle = plot(1,1);
    plot(CAR_X,CAR_Y, 'r');
    carVizHandles = visualise_car(carVizHandles, carState);
    distVizHandle = visualise_dist(distVizHandle, carState);
end

end

