% Demo Script for 

angleStart = 180;
animationON = true;
useUntunedFLC = false;

% The function will plot the trajectory by itself. No need to do anything.
[car_x, car_y] = run_car_sim(angleStart, animationON, useUntunedFLC);