function [ dataBatch ] = generate_data_batches( n, x )
%DATA_PARTITION Summary of this function goes here
%   Detailed explanation goes here

N0 = 501;
N_trn = N0+600-1;
t = N0:(N_trn);
[input, output] = pair_sample(t, x);
trnData = struct('input',input, 'output',output, 't1',t(1),'t2',t(end), 'label', 'Training');

N_val = N_trn+300;
t = (N_trn+1):N_val;
[input, output] = pair_sample(t, x);
valData = struct('input',input, 'output',output, 't1',t(1),'t2',t(end), 'label', 'Validation');

N_chk = N_val+600;
t = (N_val+1):N_chk;
[input, output] = pair_sample(t, x);
chkData = struct('input',input, 'output',output, 't1',t(1),'t2',t(end), 'label', 'Checking');

dataBatch = struct('trnData',trnData, ...
    'valData',valData, 'chkData',chkData);
end

function [input, output] = pair_sample( t, x)
% { x(t-12), x(t-6), x(t) | x(t+6) }

input = zeros(length(t), 3);
input(:,1) = x( t -  12);
input(:,2) = x( t -  6);
input(:,3) = x( t -  0);

p = 6;
output = x(t + p); 

end

