clear;

% Load Model and Data.
% load('.\models\TSK_Subclustering_BEST.mat', 'chkFis', 'db');
load('.\models\Subclustering\TSK_Subclustering_0.63.mat')
% Loads the original Time-Series samples stored in the variable named 'y'. 
load('.\data\mg24.mat'); 

% Choose Data for input
data = db.trnData;
% data = db.chkData;
% data = db.valData;

X_HAT = [];
X = [];
nn = 1;
% Multi-Step on the training data
for t=0:length(data.input)-1

    x_hat = multistep_prediction(data.input(t+1,:), chkFis);    
    x = y( (data.t1) + t + [6:6:96]);

    names{nn} = ['t=',num2str(t+data.t1)];
    nn = nn + 1;
    % Append output to array...
    X_HAT = [X_HAT; x_hat]; 
    X = [X; x];
    
end

e = (X-X_HAT).^2;
p_batch_pos = 16:16:length(X);

figure(1); clf; 
subplot(2,1,1); hold on;
plot(X); plot(X_HAT, 'r');
% stem(p_batch_pos, ones(size(p_batch_pos))*1.4, '.g');
% set(gca,'xtick',p_batch_pos-8, 'xticklabel',names); 
% legend('Prediction', 'Original', 'Prediction batches', 'Location', 'Best');
title(['Mutlistep Prediction Comparison: ', data.label, ' Data']);
xlabel('t+p, p=6,12...96');

subplot(2,1,2); hold on;
plot(e); 
% stem(p_batch_pos, ones(size(p_batch_pos))*max(e), '.g');
% set(gca,'xtick',p_batch_pos-8, 'xticklabel',names); 
xlabel('t+p, p=6,12...96');
ylabel('e^2(t+p), p=6,12...96'); 
title(['Multistep Prediction Error: ', data.label, ' Data']);


MSE = mean(e);
RMSE = sqrt(MSE);
sig = mean( (X - mean(X)).^2 );
NMSE = MSE/sig;
NDEI = sqrt(NMSE);

disp(['MSE=',num2str(MSE),' RMSE=',num2str(RMSE),' NMSE=',num2str(NMSE),' NDEI=',num2str(NDEI)]); 


