%% Evaluate TSK and Plot Stuff
clear
% load('.\models\TSK_2_linear_BP.mat')  
% load('.\models\TSK_3_linear_hybrid.mat')  
load('.\models\TSK_Subclustering_0.5.mat');

errorMetrics = evaluate_model(chkFis, db);

figure(3); clf; hold on;
plot(trnError, '*b'); plot(chkError,'r*');
if useHybrid
    title('RMSE per Iteration - Hybrid Method');
else
    title('RMSE per Iteration - BP Method');
end
ylabel('RMSE'); xlabel('Iteration');
legend('trnError', 'chkError');

figure(4);
subplot(3,1,1);  plotmf(chkFis, 'input', 1); xlabel('x(t-12)');
title(['TSK Membership Functions']);
subplot(3,1,2);  plotmf(chkFis, 'input', 2); xlabel('x(t-6)');
subplot(3,1,3);  plotmf(chkFis, 'input', 3); xlabel('x(t)');