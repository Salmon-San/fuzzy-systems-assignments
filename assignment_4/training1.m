clear;

%% Training Parameters
epochN = 200;
mfNumber = 3;
useHybrid = 1;
% TSKdegree = 'constant';
TSKdegree = 'linear';

load('.\data\mg24.mat')
db = generate_data_batches(n,y);

%% Train TSK FIS
initFis = genfis1([db.trnData.input, db.trnData.output],...
    mfNumber, 'gbellmf', TSKdegree);

trnOpt = [epochN, NaN, NaN, NaN, NaN];
dispOpt = [1, 0, 0, 1];
tic
[trnFis, trnError, stepsize, chkFis, chkError] = anfis(...
    [db.trnData.input, db.trnData.output],...
    initFis,...
    trnOpt,...
    dispOpt,...
    [db.chkData.input, db.chkData.output],...
    useHybrid);
T = toc;
disp(['Training Duration: ', num2str(T),' sec']);

%% Save Model for later use...
if useHybrid
    modelName = ['TSK_',num2str(mfNumber),'_',TSKdegree,'_hybrid'];
else
    modelName = ['TSK_',num2str(mfNumber),'_',TSKdegree,'_BP'];
end
save(['.\models\',modelName,'.mat'], 'db', 'chkFis', 'trnFis', 'chkError', 'trnError', 'useHybrid');

%% Evaluate TSK and Plot Stuff
evaluate;
