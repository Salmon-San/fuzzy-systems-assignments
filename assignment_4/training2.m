clear;

%% Training Parameters
epochN = 200;
useHybrid = 1;
TSKdegree = 'linear';
radii = 0.50; % BEST
radii = 0.50;

load('.\data\mg24.mat')
db = generate_data_batches(n,y);

%% Train TSK FIS
tic;
initFis = genfis2(db.trnData.input, db.trnData.output, radii);
T = toc;
disp(['SubClustering Duration: ', num2str(T),' sec']);
ruleNumber = length(initFis.rule);
disp(['Number of Rules Before Training: ', num2str(ruleNumber)]);


trnOpt = [epochN, NaN, NaN, NaN, NaN];
dispOpt = [1, 0, 0, 1];
tic
[trnFis, trnError, stepsize, chkFis, chkError] = anfis(...
    [db.trnData.input, db.trnData.output],...
    initFis,...
    trnOpt,dispOpt,...
    [db.chkData.input, db.chkData.output],...
    useHybrid);
T = toc;
disp(['Training Duration: ', num2str(T),' sec']);
ruleNumber = length(chkFis.rule);
disp(['Number of Rules After Training: ', num2str(ruleNumber)]);

%% Save Model for later use...
modelName = ['TSK_Subclustering_', num2str(radii)];
save(['.\models\Subclustering\',modelName,'.mat'], ...
    'db', 'chkFis', 'trnFis', 'initFis',...
    'chkError', 'trnError', 'radii');

%% Evaluate TSK and Plot Stuff
evaluate2;

