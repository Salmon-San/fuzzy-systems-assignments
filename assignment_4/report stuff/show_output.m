

N = length(chkFis.rule);
D = length(chkFis.output.mf(1).params);
output = zeros(N,D);
for i = 1:N
    output(i) = chkFis.output.mf(i).params;
    names{i} = ['rule',num2str(i)]; 
end


stem(output);
set(gca,'xtick',[1:N],'xticklabel',names)
title('Crisp TSK Output');
axis([0,9,0,max(output)*1.1])