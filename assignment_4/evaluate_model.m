function [ errorMetrics ] = evaluate_model( fis, data )
%EVALUATE_MODEL Summary of this function goes here
%   Detailed explanation goes here

%% Training Data
t_trn = data.trnData.t1:data.trnData.t2;
x_trn = evalfis( data.trnData.input, fis);
errorT = data.trnData.output - x_trn;
errTrn = calc_MSE(errorT, data.trnData);

%% Checking Data
t_chk = data.chkData.t1:data.chkData.t2;
x_chk = evalfis( data.chkData.input, fis);
errorC = data.chkData.output - x_chk;
errChk = calc_MSE(errorC, data.chkData);

%% Validation Data
t_val = data.valData.t1:data.valData.t2;
x_val = evalfis( data.valData.input, fis);
errorV = data.valData.output - x_val;
errVal = calc_MSE(errorV, data.valData);

%% Gather the results
errorMetrics = struct('trnMSE',errTrn, 'chkMSE', errChk, 'valMSE', errVal);

%% Plot Output
figure(1); clf;
subplot(3,1,1); plot(t_trn, data.trnData.output, t_trn, x_trn);
legend('Orignal Samples', 'TSK prediction');
title('Training Data - Prediction Comparison');

subplot(3,1,2); plot(t_chk, data.chkData.output, t_chk, x_chk);
legend('Orignal Samples', 'TSK prediction');
title('Checking Data - Prediction Comparison');

subplot(3,1,3); plot(t_val, data.valData.output, t_val, x_val);
legend('Orignal Samples', 'TSK prediction');
title('Validation Data - Prediction Comparison');

%% Plot Prediction Error
figure(2); clf;
subplot(3,1,1); plot(t_trn, ones(size(t_trn))*errTrn.MSE, '--r',...
    t_trn, errorT.^2);
title('Training Data - Prediction Error');
ylabel('e^2(t+6)'); xlabel('t');
legend('MSE');

subplot(3,1,2); plot(t_chk, ones(size(t_chk))*errChk.MSE, '--r',...
    t_chk, errorC.^2);
title('Checking Data - Prediction Error');
ylabel('e^2(t+6)'); xlabel('t');
legend('MSE');

subplot(3,1,3); plot( t_val, ones(size(t_val))*errVal.MSE, '--r',...
    t_val, errorV.^2);
title('Validation Data: Prediction Error');
ylabel('e^2(t+6)'); xlabel('t');
legend('MSE');
end

function errStruct = calc_MSE(error, data)

MSE = mean(error.^2);
RMSE = sqrt(MSE);
sig_x = mean( (data.output - mean(data.output)).^2 );
NMSE = MSE/sig_x;
NDEI = sqrt(NMSE);

errStruct = struct('MSE', MSE, 'RMSE',RMSE, 'NMSE',NMSE, 'NDEI', NDEI);

end



