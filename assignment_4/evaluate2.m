%% Evaluate TSK and Plot Stuff
clear

% load('.\models\TSK_Subclustering_BEST.mat')
load('.\models\Subclustering\TSK_Subclustering_0.63.mat');

ClusterNumber = length(chkFis.rule);

errorMetrics = evaluate_model(chkFis, db);

figure(3); clf; hold on;
plot(trnError, '*b'); plot(chkError,'r*');
title(['RMSE per Iteration - ',num2str(ClusterNumber),' Clusters']);
ylabel('RMSE'); xlabel('Iteration');
legend('trnError', 'chkError');

return
%% Plot MFs
figure(4); clf;
[x1,mf1] = plotmf(initFis, 'input', 3); 
[x2,mf2] = plotmf(chkFis, 'input', 3); xlabel('x(t)');


subplot(3,1,1);  hold on;
plot(x1(:,1:3),mf1(:,1:3)); 
plot(x2(:,1:3),mf2(:,1:3)); 
title('TSK with Clustering. MFs Before and After Training');
legend('in3cluster1','in3cluster2','in3cluster3');

subplot(3,1,2);  hold on;
plot(x1(:,4:6),mf1(:,4:6)); 
plot(x2(:,4:6),mf2(:,4:6));
legend('in3cluster4','in3cluster5','in3cluster6');

subplot(3,1,3);  hold on;
plot(x1(:,6:9),mf1(:,6:9)); plot(x2(:,6:9),mf2(:,6:9));
legend('in3cluster7','in3cluster8','in3cluster9');
xlabel('x(t)');
