function [ y ] = multistep_prediction( input, chkFis)
%MULTISTEP Summary of this function goes here
%   Detailed explanation goes here

P = 96/6;
y = zeros(P,1);

for i=1:P
    y(i) = evalfis(input,chkFis);
    
    % Shift Input Data
    input(1) = input(2);
    input(2) = input(3);
    input(3) = y(i);
    
end

end

