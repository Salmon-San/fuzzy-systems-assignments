clear;

%% Init Fuzzy Engine Elements
FZPI = newfis('FZPI');
FZPI.andMethod = 'min';
FZPI.impMethod = 'prod';

%% Fuzzy-Set Partitioning of INPUT E
A = 1; %60;
FZPI = addvar(FZPI, 'input', 'E', A*[-1,1]); 

T = [-1/2, 0, +1/2]*A;
FZPI = addmf(FZPI, 'input', 1, 'NL', 'trimf', A*[0,0,1/2] - A);
FZPI = addmf(FZPI, 'input', 1, 'NM', 'trimf', T - 2*A/3);
FZPI = addmf(FZPI, 'input', 1, 'NS', 'trimf', T - A/3);
FZPI = addmf(FZPI, 'input', 1, 'ZE', 'trimf', T + 0 );
FZPI = addmf(FZPI, 'input', 1, 'PS', 'trimf', T + A/3);
FZPI = addmf(FZPI, 'input', 1, 'PM', 'trimf', T + 2*A/3);
FZPI = addmf(FZPI, 'input', 1, 'PL', 'trimf', A*[-1/2,0,0] + A);


%% Fuzzy-Set Partitioning of INPUT dE
A = 1; %120;
FZPI = addvar(FZPI, 'input', 'DE', A*[-1,1]); 
T = [-3/8, 0, +3/8]*A;
FZPI = addmf(FZPI, 'input', 2, 'NV', 'trimf', A*[0,0,1/4] - A);
FZPI = addmf(FZPI, 'input', 2, 'NL', 'trimf', T - 3*A/4);
FZPI = addmf(FZPI, 'input', 2, 'NM', 'trimf', T - 2*A/4);
FZPI = addmf(FZPI, 'input', 2, 'NS', 'trimf', T - A/4);
FZPI = addmf(FZPI, 'input', 2, 'ZE', 'trimf', T + 0 );
FZPI = addmf(FZPI, 'input', 2, 'PS', 'trimf', T + A/4);
FZPI = addmf(FZPI, 'input', 2, 'PM', 'trimf', T + 2*A/4);
FZPI = addmf(FZPI, 'input', 2, 'PL', 'trimf', T + 3*A/4);
FZPI = addmf(FZPI, 'input', 2, 'PV', 'trimf', A*[-1/4,0,0] + A);

%% Fuzzy-Set Partitioning of OUTPUT dU
A = 1;
FZPI = addvar(FZPI, 'output', 'DU', A*[-1,1]);

T = [-3/8, 0, +3/8]*A;
FZPI = addmf(FZPI, 'output', 1, 'NV', 'trimf', A*[0,0,1/4] - A);
FZPI = addmf(FZPI, 'output', 1, 'NL', 'trimf', T - 3*A/4);
FZPI = addmf(FZPI, 'output', 1, 'NM', 'trimf', T - 2*A/4);
FZPI = addmf(FZPI, 'output', 1, 'NS', 'trimf', T - A/4);
FZPI = addmf(FZPI, 'output', 1, 'ZE', 'trimf', T + 0 );
FZPI = addmf(FZPI, 'output', 1, 'PS', 'trimf', T + A/4);
FZPI = addmf(FZPI, 'output', 1, 'PM', 'trimf', T + 2*A/4);
FZPI = addmf(FZPI, 'output', 1, 'PL', 'trimf', T + 3*A/4);
FZPI = addmf(FZPI, 'output', 1, 'PV', 'trimf', A*[-1/4,0,0] + A);


%% Fuzzy Rules
% The following code generates automatically the rule Base. More details
% regarding the rule-table is in the report. 

% Rule Table
NV=1; NL=2; NM=3; NS=4; ZE=5; PS=6; PM=7; PL=8; PV=9;
MF_VAL = [-4:4]; % MF_VAL(ZE)= 0;

ruleTable = zeros(9,7);

% Rule List - IF-Part is set.
ruleList = zeros(9*7, 2+1+2);
ruleList(:,4:5) = 1;
k = 1;
for i=1:9
    for j=1:7
        % Meta-Rule Implementation
        dE_val = MF_VAL(i);
        E_val = MF_VAL(j+1);
        dU_val =  dE_val + E_val;
        
        if dU_val < -4
            dU_val = -4;
        elseif dU_val > 4
            dU_val = +4;
        end
        
        ruleTable(i,j) = dU_val;
        
        % Add Rule to FIS
        ruleList(k,1:2) = [j,i];
        ruleList(k,3) = ruleTable(i,j)+5;
        k = k + 1;
    end
end

ruleTable(ruleTable<-4) = -4;
ruleTable(ruleTable>+4) = +4;

FZPI = addrule(FZPI, ruleList);
showrule(FZPI)
gensurf(FZPI)

writefis(FZPI, 'sattFZPI');












