function [ Tr, t1, t2 ] = calc_rise_time( x, t, target )
%CALC_RISE_TIME calculates the rise time of a given signal (10%-90%
%criterion).
%   The input signal needs to contain the Steady-State value as its last
%   element.
%   This fun returns the approximated t1 and t2 instances of the 10% and
%   90% signals

x_ = x/target;

for i=1:length(x_)
   if x_(i) > 0.1
       t1 = i;
       break;
   end
end

for j=i:length(x_)
   if x_(j) > 0.9
       t2 = j;
       break;
   end
end


% clf; hold on;
% plot(x_);
% 

Tr = t(t2)-t(t1);
end

