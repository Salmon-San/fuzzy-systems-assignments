function [ OV, i ] = calc_overshoot( x, target )
%CALC_OVERSHOOT estimated the overshoot of a typical step-response signal.
%   x = the output signal of the system in vector form [1,N] or [N,1]
%   target is the Step Amptitude of the input signal, and is use to
%   calculate the percentage % of the overshoot

[X,i] = max(x);

OV = (X-target)/target*100;
OV = round(100*OV)/100; 
end

