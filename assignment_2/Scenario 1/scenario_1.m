% This script simulates and demonstrates the step response of a Classical
% Linear PI controller adn two PI FLC -one fully tuned and one with the initial
% gains from its linear counterpart.

% The script utilises the scenario_2_sim.slx as well as the 
% FuzzySattelite.slx and ClassicSattelite.slx files in the 'Models' folder. 
% Both of these files need to be included in the matlab path. 

clear;
% Load FIS 
FZPI = readfis('sattFZPI');

%% Run Simulink Simulation 
simOut = sim('scenario_1_sim', ...
            'StartTime', '0', ...
            'StopTime', '5' );
tout=  simOut.get('tout');
yout = simOut.get('yout');

% Calc Rise Time
target = 60; % Step function reference Amptitude

[ Tr1, m1, m2 ] = calc_rise_time( yout(:,1), tout, target  ); % Classic PI
[ Tr2, n1, n2 ] = calc_rise_time( yout(:,2), tout, target  ); % Untuned FZPI
[ Tr3, k1, k2 ] = calc_rise_time( yout(:,3), tout, target  ); % Tuned FZPI

% Calc Overshoot of each Response
[MP1, m3] = calc_overshoot(  yout(:,1), target  );
[MP2, n3] = calc_overshoot(  yout(:,2), target  );
[MP3, k3] = calc_overshoot(  yout(:,3), target  );

%% Show results and do the victory dance
figure(1); clf; hold on;
% plot(tout, yout(:,1:2));
plot(tout, yout);

% A viz of the Rise Time and Overshoot
stem( tout([m1,m2,m3]), yout([m1,m2,m3],1), '-*c');
stem( tout([n1,n2,n3]), yout([n1,n2,n3],2), '-*g');
stem( tout([k1,k2,k3]), yout([k1,k2,k3],3), '-*m');

% A couple of helpful dashed lines 
plot(tout(1:10:end), 0.9*target *ones(size(tout(1:10:end))), 'k--'); 
plot(tout(1:10:end), target*ones(size(tout(1:10:end))), 'k--'); 

% Print Response Criterions
legend( ['Linear PI, ',       't_r = ', num2str(Tr1),   ', MP% = ', num2str(MP1),'%'], ... 
        ['FZ-PI untuned, ',   't_r = ', num2str(Tr2),   ', MP% = ', num2str(MP2),'%'], ...
        ['FZ-PI tuned, ',     't_r = ', num2str(Tr3),   ', MP% =', num2str(MP3),'%'], ...
        'Location', 'Best');
% legend('Linear PI', 'FZ-PI Untuned', 'FZ-PI Tuned');

title('Step Response');
xlabel('time [sec]');
ylabel('Sattelite Angle y(t) [deg]');

