% This Script visualizes the effects that the gains of a PI-FLC have on the
% system's response. 
% The script utilises the FZPI_calibration_demo_sim.slx as well as the 
% FuzzySattelite.slx simulink model int the Models folder. Both of these
% files need to be included in the matlab path. 

clear;
% Load FIS
FZPI = readfis('sattFZPI');

%% Compare K_e and alpha
Ke_set = [0.5,1,3, 5,10,20];
a_set = [0.2:0.1:0.7];

figure(1); clf; color = {'b','g','r','m','c','k'}; 

subplot(1,2,1); hold on; i=1;
Ke_ = 1; a_ = 0.5; Ku_ = 2;
for Ke_ = Ke_set
    sim('FZPI_calibration_demo_sim');
    plot(tout, yout, color{i});
    i = i + 1;
end
title('Effect of K_e (�=0.5, K_u=2)');
str='K_e='; set = Ke_set; 
legend([str,num2str(set(1))],[str,num2str(set(2))],[str,num2str(set(3))],...
    [str,num2str(set(4))],[str,num2str(set(5))],[str,num2str(set(6))],...
    'Location', 'Best');
ylabel('y(t) [deg]'); xlabel('time [sec]');

subplot(1,2,2); hold on; i=1;
Ke_ = 1; a_ = 0.5; Ku_ = 2;
for a_ = a_set
    sim('FZPI_calibration_demo_sim');
    plot(tout, yout, color{i});
    i = i + 1;
end
title('Effect of � (K_e=1, K_u=2)');  
str='�='; set = a_set;  
legend([str,num2str(set(1))],[str,num2str(set(2))],[str,num2str(set(3))],...
    [str,num2str(set(4))],[str,num2str(set(5))],[str,num2str(set(6))],...
    'Location', 'Best');
ylabel('y(t) [deg]'); xlabel('time [sec]');


%% Figure
Ku_set = [1,5,10, 20,40,80];
figure(2); hold on; i=1;
Ke_ = 1; a_ = 0.5;
for Ku_ = Ku_set
    sim('FZPI_calibration_demo_sim');
    plot(tout, yout, color{i});
    i = i + 1;
end
title('Effect of K_u, (K_e=1, �=0.5)');
str='K_u='; set = Ku_set; 
legend([str,num2str(set(1))],[str,num2str(set(2))],[str,num2str(set(3))],...
    [str,num2str(set(4))],[str,num2str(set(5))],[str,num2str(set(6))],...
    'Location', 'Best');
ylabel('y(t) [deg]');
xlabel('time [sec]');
% Ke_ = 2;
% a_ = 0.4;
% Ku_ = 25;
% sim('FZPI_sattelite_sim');
% plot(tout,yout);
% 
% for

