% This Script Calculates the defuzzified inference of the FLS's Fuzzy
% Inference Engine. 

figure(1); clf; 

x = -1:0.001:1;
xx = [-0.5,0,0.5];
NV = trimf(x, xx - 1.0)*1/3;
NL = trimf(x, xx - 3/4)*2/3;
NM = trimf(x, xx - 2/4)*2/3;
NS = trimf(x, xx - 1/4);
ZR = trimf(x, xx - 0.0)*2/3;
PS = trimf(x, xx + 1/4)*2/3;
PM = trimf(x, xx + 2/4)*1/3;
PL = trimf(x, xx + 3/4)*1/3;
plot(x,[NV',NL',NM', NS',ZR', PS',PM',PL']);
xlabel('DU');


DU = max([NV',NL',NM', NS',ZR', PS',PM',PL']');
COA = sum(x.*DU)/sum(DU);
plot(x, DU); hold on;
stem(COA,  1,'r');
title('Fuzzy Inference');
xlabel('DU');
ylabel('�_C^''(z)');
legend('Fuzzy Inference MF', 'De-fuzzified Inference');