% This script simulates the controll system for various desired angle
% trajectories adn demonstrates the output and error signal.

% The script utilises the scenario_2_sim.slx as well as the 
% FuzzySattelite.slx and ClassicSattelite.slx files in the 'Models' folder. 
% Both of these files need to be included in the matlab path. 

clear;
% Load FIS 
FZPI = readfis('sattFZPI');

%% Run Simulation for First Signal - Mutliple Step functions

% Construct the reference signal
t1 = [0:0.001:4]';
t2= t1 + t1(end)+0.001;
t3= t1 + t2(end)+0.001;
temp = ones(size(t1));
yd = [t1,60*temp; 
      t2,20*temp; 
      t3,40*temp];

% simulate
simOut = sim('scenario_2_sim', 'StartTime', '0', 'StopTime', '15' );
tout=  simOut.get('tout');
yout = simOut.get('yout');

% Show results 
figure(1); clf; 
plot(tout, yout); % Plot linear PI, Fuzzy PI, and the reference signal. 
legend('Linear PI', 'FZ-PI', 'Desired Angle');
title('Succcesive Steps Response');
xlabel('time [sec]');
ylabel('Sattelite Angle y(t) [deg]');


%% Run Simulation for Second Signal - Ramp-Up and Ramp-Down

% Construct the reference signal
t1 = [0:0.001:5]';
t2= [5.001:0.001:8]';
t3= [8.001:0.001:16]';
t4 = [16.001:0.001:20]';
yd = [t1,60*t1/5; 
      t2,60*ones(size(t2)); 
      t3,-60*(t3-8)/8+60;
      t4, zeros(size(t4))];

% simulate
simOut = sim('scenario_2_sim', 'StartTime', '0', 'StopTime', '20' );
tout=  simOut.get('tout');
yout = simOut.get('yout');

% Show results 
figure(2); clf; 
plot(tout, yout); % Plot linear PI, Fuzzy PI, and the reference signal. 
legend('Linear PI', 'FZ-PI', 'Desired Angle');
title('Ramp Response');
xlabel('time [sec]');
ylabel('Sattelite Angle y(t) [deg]');

figure(3); clf; 
plot(tout, yout(:,1)-yout(:,3), tout, yout(:,2)-yout(:,3));
% plot(tout, tout, yout(:,2)-yout(:,3));
title( 'Error Signals');
legend('Linear PI', 'FZ-PI');
xlabel('time [sec]');
ylabel('Sattelite Error Angle y(t)-y_d(t) [deg]');


%% Simulate some more: Sinusoidal

% Construct the reference signal
t = [0:0.001:15]';
yd = [t,60+20*sin(2*pi*t/5)];

% simulate
simOut = sim('scenario_2_sim', 'StartTime', '0', 'StopTime', '15' );
tout=  simOut.get('tout');
yout = simOut.get('yout');

% Show results 
figure(4); clf; 
plot(tout, yout); % Plot linear PI, Fuzzy PI, and the reference signal. 
legend('Linear PI', 'FZ-PI', 'Desired Angle');
title('Sinusodial Response');
xlabel('time [sec]');
ylabel('Sattelite Angle y(t) [deg]');



