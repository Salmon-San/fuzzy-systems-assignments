clear;

% Plant TF
Gp = tf(10, [1,10,9]); 

% Controller TF
Kp = 1; c = 2;
Gc = tf([Kp, Kp*c], [1,0]);

% Open-Loop TF
L = Gp*Gc;

%% Root Locus Analysis 
figure(1);
rlocus(L); %, [0:0.001:4])
K = 0.8; % THIS IS IT

% Closed-Loop TF
H = feedback(L*K, 1, -1);

%% Selected Gain 
Kp = K;
Ki=Kp*c;
disp(['The PI Controller Gains are Kp = ', num2str(Kp),' and Ki = ',num2str(Ki)]);

%% Time Domain Analysis
figure(2);
subplot(1,2,1); 
t = 0:0.01:20;
u = 60*ones(size(t));
[y,t] = lsim(H,u,t);
plot(t,y, t,u);
title('Step Response');
legend('y(t)' ,' r(t)', 'Location', 'Best');
grid on;
% step(H);

subplot(1,2,2);
T = 30;
t = 0:0.01:T;
u = t;
[y,t] = lsim(H,u,t);
plot(t,y,t,u, t, y'-u);
title('Ramp Response');
legend('y(t)' ,' r(t)', 'e(t)' ,'Location', 'Best');
grid on;