
%% Partitions of each Fuzzy Variable
% Each vector defines a triangular MF.
NS1 = [-6,-5,0]; ZE1 = [-5,0,+5]; PS1 = [0,+5,6]; % Partition of X1
NS2 = [-91,-90,0]; ZE2 = [-90,0,+90]; PS2 = [0,+90,91]; % Partition of X2
ZEy = [-1,0,1]; PSy = [-0,1,+2]; PMy = [1,+2,3]; PBy = [2,+3,4]; % Partition of Y

%% Rule DataBase
% The rules are given to the Inference Engine, for better performance. This
% whole process is purely optional.
Rule(1) = struct('X1_MF',NS1, 'X2_MF',NS2, 'Y_MF',PBy);
Rule(2) = struct('X1_MF',NS1, 'X2_MF',ZE2, 'Y_MF',PBy);
Rule(3) = struct('X1_MF',NS1, 'X2_MF',PS2, 'Y_MF',PBy);
Rule(4) = struct('X1_MF',ZE1, 'X2_MF',NS2, 'Y_MF',PBy);
Rule(5) = struct('X1_MF',ZE1, 'X2_MF',ZE2, 'Y_MF',PMy);
Rule(6) = struct('X1_MF',ZE1, 'X2_MF',PS2, 'Y_MF',PSy);
Rule(7) = struct('X1_MF',PS1, 'X2_MF',NS2, 'Y_MF',PSy);
Rule(8) = struct('X1_MF',PS1, 'X2_MF',ZE2, 'Y_MF',PSy);
Rule(9) = struct('X1_MF',PS1, 'X2_MF',PS2, 'Y_MF',PSy);

%% Accessing the whole X1-X2 space
x1 = linspace(-5,+5,20);
x2 = linspace(-90,+90,20);
[X1,X2] = meshgrid(x1,x2);
[M,N] =size(X1);
Y = zeros(M,N);
for i=1:M
    for j=1:N
        Y(i,j) = fuzzyInferenceEngine(X1(i,j),X2(i,j), 0, Rule);
    end
    
end

figure(1)
mesh(X1,X2,Y);
title('De-Fuzzified Output');
xlabel('X_1 [mmHg]');
ylabel('X_2 [mmHg*sec]');
zlabel('Y %');

