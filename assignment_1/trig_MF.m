function [ MF ] = trig_MF( X, MF_params)
%TRIGMF Summary of this function goes here
%   Detailed explanation goes here
MF = zeros(size(X));
a = MF_params(1);
b = MF_params(2);
c = MF_params(3);

if a==b
    for i=1:length(X)
        if X(i) < a || X(i) > c
            MF(i) = 0;
            %         elseif X(i) < b
            %             MF(i) = (X(i)-a)/(b-a);
        else
            MF(i) = (c-X(i))/(c-b);
        end
    end
elseif b==c
    for i=1:length(X)
        if X(i) < a || X(i) > c
            MF(i) = 0;
        else
            %         elseif X(i) < b
            MF(i) = (X(i)-a)/(b-a);
            %         else
            %             MF(i) = (c-X(i))/(c-b);
        end
    end
else
    for i=1:length(X)
        if X(i) < a || X(i) > c
            MF(i) = 0;
        elseif X(i) < b
            MF(i) = (X(i)-a)/(b-a);
        else
            MF(i) = (c-X(i))/(c-b);
        end
    end
end
end

