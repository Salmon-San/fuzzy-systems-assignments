function [ y ] = fuzzyInferenceEngine( x1, x2, Viz, Rules )
%y = FUZZYENGINE(x1,x2,Rules, Viz) calculates the de-fuzzified output of a fuzzy inference
%engine of 2 input variables and 1 output.
%   x1,x2: The input.
%   Viz: [OPTIONAL] 0 or 1 enables the visualisation of the inferred fuzzy
%   sets of each rule and total fuzzy output composed with the ALSO
%   operator. Default is 0
%   Rules: [OPTIONAL] is a struct, that contains the trig_MF parameters of
%   each fuzzy set that participates to the rule. The rules for the MAP
%   control are stored in this function as well, but different rules or the
%   same rules can be given as arguments, increasing performance.
%
%   Rule Example:
%       'R1: IF x1 is NS1 AND x2 is NS2 THEN y is PBy' is coded as
%        Rules(1) = struct('X1_MF',NS1, 'X2_MF',NS2, 'Y_MF',PBy);
%       where NS1,NS2 PBy are 1D vectors containing the [a,bc,c]
%       paramterers of each transfer fun.

%% Constract Rule-Database

if nargin <= 3
    NS1 = [-5,-5,0]; ZE1 = [-5,0,+5]; PS1 = [0,+5,5]; % Partition of X1
    NS2 = [-90,-90,0]; ZE2 = [-90,0,+90]; PS2 = [0,+90,90]; % Partition of X2
    ZEy = [0,0,1]; PSy = [-0,1,+2]; PMy = [1,+2,3]; PBy = [2,+3,3]; % Partition of Y
    Rules(1) = struct('X1_MF',NS1, 'X2_MF',NS2, 'Y_MF',PBy);
    Rules(2) = struct('X1_MF',NS1, 'X2_MF',ZE2, 'Y_MF',PBy);
    Rules(3) = struct('X1_MF',NS1, 'X2_MF',PS2, 'Y_MF',PBy);
    Rules(4) = struct('X1_MF',ZE1, 'X2_MF',NS2, 'Y_MF',PBy);
    Rules(5) = struct('X1_MF',ZE1, 'X2_MF',ZE2, 'Y_MF',PMy);
    Rules(6) = struct('X1_MF',ZE1, 'X2_MF',PS2, 'Y_MF',PSy);
    Rules(7) = struct('X1_MF',PS1, 'X2_MF',NS2, 'Y_MF',PSy);
    Rules(8) = struct('X1_MF',PS1, 'X2_MF',ZE2, 'Y_MF',PSy);
    Rules(9) = struct('X1_MF',PS1, 'X2_MF',PS2, 'Y_MF',PSy);
end

%% Individual Rule Inference
% As specified by the assignment guidelines
yy = linspace(0,3,100); % Sampling of the output Y-space.
w = zeros(length(Rules),1); % DOF of each rule
ruleInference_MF = zeros(length(Rules), length(yy));
for i=1:length(Rules)
    
    % Degree of Fullfillment of Rule(i) with Singleton Fuzzyfier
    w1 = trig_MF(x1, Rules(i).X1_MF);
    w2 = trig_MF(x2, Rules(i).X2_MF);
    w(i) = min(w1,w2);
    % Each DOF of each Rule is stored in an array fot the De-Fuzzifying process
    
    % Fuzzy Inference of Rule(i)
    % MF_Y(y)_inferred = min(w,MF_Y(y)).
    yMF = trig_MF(yy, Rules(i).Y_MF);
    ruleInference_MF(i,:) = min(w(i),yMF);
    % ruleInference_MF is the Membership Function of each THEN-part,
    % modified by the DOF of the rule
end

%% De-fuzzufier
y_centers = zeros(length(Rules),1);
for i=1:length(Rules)
    y_centers(i) = (Rules(i).Y_MF(1)+Rules(i).Y_MF(3))/2;
end
y = sum(w.*y_centers)/sum(w);

%% Visualization

% The visualization flag
if nargin == 2
    Viz = 0;
end

if Viz
    % plot each Rule's inferred Fuzzy Output
    figure(1); clf;
    Y_NAME = {'PB','PB','PB', 'PB','PM', 'PS', 'PS','PS','PS'};
    for i=1:length(Rules);
        subplot(3,3,i);
        yMF = trig_MF(yy, Rules(i).Y_MF);
        hold on;
        plot(yy, yMF, 'g')
        area(yy,ruleInference_MF(i,:));
        
        title(['Fuzzy Inference of Rule ',num2str(i)]);
        legend(texlabel(['MF(y)_',Y_NAME{i}]),'Location', 'Best');
    end
    
    % ALSO-Composition. This is not needed, but is is calculated anyway for
    % demonstration purposes.
    figure(2); clf; hold on;
    totalFuzzyOutput = zeros(size(yy));
    for i=1:length(yy)
        totalFuzzyOutput(i) = max(ruleInference_MF(:,i));
    end
    subplot(1,2,1); plot(yy,totalFuzzyOutput); title('ALSO-Composed Fuzzy Output');
    subplot(1,2,2); plot(yy,ruleInference_MF, '--'); title('Output of each Rule');
end

end